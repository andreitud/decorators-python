# A basic decorator
def safe_runner(function):
  """ The function `safe_runner` will be used to decorate after functions.
  The function `wrapper`, declared locally, will replace the `function`
  argument as the final "overloaded"version. In other words, after decorating 
  `function`, calling `function` will actually result in calling `wrapper` 
  (which calls `function` in its definition; fun = safe_runner(fun)).

  This decorator will work without parameters.
  """
  def wrapper(*args, **kwargs):
    try:
      print('args: ',*dargs)
      return function(*args, **kwargs)
    except Exception as e:
      print(f'Function {function.__name__} had {e.__class__.__name__}: {e}')

  return wrapper


@safe_runner
def divizor(a, b):
  return a/b


# First implementation of a parametrized decorator
def plain_decorator(*dargs):
  """Normally, when declaring a decorator, one must expect the first
  parameter to be the decorator function. Here was the attempt of a
  parametrized decorator, expecting something like @decorator(param1, param2)
  to work. This was not de case as the decorator function acted as a normal
  function call and the decorated function was not passed as parameter.
This decorator will work only
  This decorator will work without parameters.
  """
  print('decorator args >>', dargs)
  function = lambda *aaa, **kwg:  f'{aaa} - {list(kwg.keys())}'
  for fun in [e for e in dargs if callable(e)]:
    function = fun
    break

  def wrapper(*args, **kwargs):
    try:
      print('b4')
      return function(*args, **kwargs)
    finally:
      print('after')
      
  return wrapper


@plain_decorator
def getter(lili, index):
  return lili[index]


# Decorator incompatible with this case
@plain_decorator('uno', 'dos', {'id':77})
def bringer(didi, key):
  return didi[key]


# Second implementation of a parametrized decorator
def shout_something(what_to_say):
  """Using three functions: outer decorator (that takes parameters),
  inner decorator (which requires the function),
  wrapper (the actual overload of the function).

  This decorator will only work when parameters will be given.
  """

  def shout_something_decorator(function):
    def wrapper(*args, **kwargs):
      print(f'shoting {what_to_say} for {function.__name__}')
      return function(*args, **kwargs)

    return wrapper

  return shout_something_decorator


@shout_something('nanii')
def printer(message):
  print(f'Function: {message}')


# Decorator incompatible with this case
@shout_something
def new_fun(*args):
  didi = {}
  for i, e in enumerate(*args):
          didi[i]=e
  return didi


# Final parametrized decorator
def fun_return_has_type(_type=type(None)):
  """
  """
  def fun_return_decorator(function):
    def wrapper(*args, **kwargs):
      ret = function(*args, **kwargs)
      print(f'Value {ret} of function {function.__name__} has return type '\
        f'{type(ret).__name__}.')
      return ret

    return wrapper

  # If the decorator is unparametrized then the first
  # argument will be the function (which is callable)
  if callable(_type):
    function = _type
    _type = type(None)
    # For that reason, in order to decorate as intended,
    # the wrapper must be returned
    return fun_return_decorator(function)
  else:
    # If a parameter is given to the decorator then the
    # inner function must be returned as normal
    return fun_return_decorator
  




