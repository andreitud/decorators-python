DROP PROCEDURE IF EXISTS give_tables;
-- ~~
create procedure give_tables()
BEGIN
  DECLARE table_name VARCHAR(255);

  DECLARE done INT DEFAULT FALSE;
  DECLARE tables_cursor CURSOR FOR 
    SELECT table_name FROM information_schema.tables 
    WHERE table_schema='hyundai_develop';
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN tables_cursor;
  za_loop:LOOP
    FETCH tables_cursor INTO table_name;
    IF done THEN
      LEAVE za_loop;
    END IF;

    SELECT COUNT(*) FROM table_name;
  END LOOP;
  CLOSE tables_cursor;
END;

call give_tables;

SELECT concat('select count(*) from ', table_name)  FROM information_schema.tables 
    WHERE table_schema='hyundai_develop';

select count(*) from admins	;
select count(*) from alembic_version	;
select count(*) from answers	;
select count(*) from cameras	;
select count(*) from car_models	;
select count(*) from change_password_tokens	;
select count(*) from cobrowsing_sessions	;
select count(*) from customer_attendance	;
select count(*) from customers	;
select count(*) from customers_to_presentations	;
select count(*) from device_tokens	;
select count(*) from exchanges	;
select count(*) from locations	;
select count(*) from mail_tokens	;
select count(*) from media	;
select count(*) from messages	;
select count(*) from moderator_attendance	;
select count(*) from moderators	;
select count(*) from powerbi_report	;
select count(*) from presentation_121_queue	;
select count(*) from presentation_to_camera	;
select count(*) from presentations	;
select count(*) from presetations_to_moderators	;
select count(*) from questions	;
select count(*) from rooms	;
select count(*) from settings	;
select count(*) from status_changes	;
select count(*) from stream_statuses	;
select count(*) from streams	;
select count(*) from sys_admins	;
select count(*) from users;
